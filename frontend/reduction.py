#!/usr/bin/env python
import simplejson as json
import sys
import math
if len(sys.argv)<4:
	print "Usage: reduction.py [input] [output] [threshold]"
	exit()
f=open(sys.argv[1])
js=json.loads(f.read())
first=True
temp=[0,0]
frames=js.get(js.keys()[0]).get('frames')
for k in js.get(js.keys()[0]).get('frames').keys():
	if first:
		temp=[frames.get(k)[0],frames.get(k)[1]]
		first=False
	else:

		if math.fabs(math.sqrt(math.pow(temp[0],2) + math.pow(temp[1],2)) - math.sqrt(math.pow(frames.get(k)[0],2)+math.pow(frames.get(k)[1],2))) > int(sys.argv[3]):
			temp=[frames.get(k)[0],frames.get(k)[1]]
		else:
			frames.get(k)[0]=temp[0]
			frames.get(k)[1]=temp[1]
#js.get(js.keys()[0]).get('frames')=frames
f=open(sys.argv[2],'w+')
#print frames
f.write(json.dumps({js.keys()[0]:{js.get(js.keys()[0]).keys()[0]:{key:frames.get(key) for key in frames.keys()}}}))
f.close()
